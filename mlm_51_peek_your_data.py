import numpy
from pandas import read_csv
from pandas import set_option
from pandas.tools.plotting import scatter_matrix

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

filename = 'pima-indians-diabetes.data.csv'
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi',
         'age', 'class']
data = read_csv(filename, names=names)

peek = data.head(20)
types = data.dtypes

set_option('display.width', 100)
set_option('precision', 3)

description = data.describe()
print("\ndata description")
print(description)

print("\nclass distribution")
class_counts = data.groupby('class').size()
print(class_counts)

print("\nPairwise Pearson correlations:")
correlations = data.corr(method='pearson')
print(correlations)

print("\nSkew for each attribute")
skew = data.skew()
print(skew)

fig = plt.figure()
ax = fig.add_subplot(111)
data.hist(ax=ax)
plt.tight_layout()
fig.savefig('histogram.png')

data.plot(kind='density', subplots=True, layout=(3, 3), sharex=False, ax=ax)
fig.savefig('density.png')

data.plot(kind='box', subplots=True, layout=(3, 3), sharex=False,
          sharey=False, ax=ax)
fig.savefig('box_whiskers.png')

# plot correlation matrix
correlations = data.corr()
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(correlations, vmin=-1, vmax=1)
fig.colorbar(cax)
ticks = numpy.arange(0, 9, 1)
ax.set_xticks(ticks)
ax.set_yticks(ticks)
ax.set_xticklabels(names)
ax.set_yticklabels(names)
fig.savefig('correlation.png')

correlations = data.corr()
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(correlations, vmin=-1, vmax=1)
fig.colorbar(cax)
fig.savefig('correlation_generic.png')

fig = plt.figure()
ax = fig.add_subplot(111)
scatter_matrix(data, ax=ax)
fig.savefig('scatter_matrix.png')
