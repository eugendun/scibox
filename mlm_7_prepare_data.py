# data preparation:
# 1. rescale
# 2. standardize
# 3. normalize
# 4. binarize

# rescale data between 0 and 1
from pandas import read_csv
from numpy import set_printoptions
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import Binarizer

filename = 'pima-indians-diabetes.data.csv'
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi',
         'age', 'class']
dataframe = read_csv(filename, names=names)
array = dataframe.values

print('raw input:')
print(dataframe.describe())

X = array[:,0:8]
Y = array[:,8]
scaler = MinMaxScaler(feature_range=(0, 1))
rescaledX = scaler.fit_transform(X)
set_printoptions(precision=3)
print(rescaledX[0:5, :])

# standardize
# It is most suitable for techniques that assume a Gaussian distribution
# in the input variables and work better with rescaled data, such as lear regression
# logistic regression and linear discriminate analysis.
scaler = StandardScaler().fit(X)
rescaledX = scaler.transform(X)
print(rescaledX[0:5, :])

# normalize
# scikit-learn refers to rescaling each observation (row) to have a length of 1
# useful for sparse datasets with attributes of varying scaled when using algorithms
# that weight input values such as neural networks and algorithms that use distance
# measures such as k-Nearest Neightbors
scaler = Normalizer().fit(X)
normalizedX = scaler.transform(X)
print(normalizedX[0:5, :])

# binarize (also called thresholding)
# all values above a threshold are marked 1 and all equal or below are marked as 0
# useful when feature engingeering
binarizer = Binarizer(threshold=0.0).fit(X)
binaryX = binarizer.transform(X)
print(binaryX[0:5, :])


