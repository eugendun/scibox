## Spot-Check Classification Algorithms
# Spot-checking is a way of discovering wich algorthms perform well on
# your machine learning problem. You cannot know which algorithms are
# best suited to you problem beforehand. You must trial a number of
# methods and focus attention on those that prove themselves the most
# promising.

## Algorthims Overview
# six classification algorthims:
#  two linear machine learning algorithms
#   * Logistic Regression
#   * Linear Discriminant Analysis
#  four nonlinear machine learning algorithms
#   * k-Nearest Neighbors
#   * Naive Bayes
#   * Classification and Regression Trees
#   * Support Vector Machines

# imports
from pandas import read_csv
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC

# load data
filename = 'pima-indians-diabetes.data.csv'
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi',
         'age', 'class']
dataframe = read_csv(filename, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]

## Logistic Regression
num_folds = 10
kfold = KFold(n_splits=10, random_state=7)
model = LogisticRegression()
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

## Linear Discriminant Analysis - LDA
model = LinearDiscriminantAnalysis()
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

## k-Nearest Neighbors
model = KNeighborsClassifier()
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

## Naive Bayes
model = GaussianNB()
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

## Classification and Regression Trees - CART
model = DecisionTreeClassifier()
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

## Support Vector Machines - SVM
# Of particular importance is the use of different kernel functions
# via the kernel parameter. A powerfull Radial Basis Function is used
# by default.
model = SVC()
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())
