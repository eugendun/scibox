import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy

fig = plt.figure()

# line plot
ax = fig.add_subplot(121)
ax.plot([1,2,3])
plt.xlabel('some x axis')
plt.ylabel('some y axis')

# scatter plot
x = numpy.array([1,2,3])
y = numpy.array([2,4,6])
bx = fig.add_subplot(122)
bx.scatter(x,y)
plt.xlabel('some x axis')
plt.ylabel('some y axis')

# plt.show()
fig.savefig('test.png')

