## Classification Metrics:
# - Classification Accuracy
# - Logarithmic Loss
# - Area Under ROC Curve
# - Confusion Matrix
# - Classification Report

## Regression Metrics:
# - Mean Absolute Error
# - Mean Squared Error
# - R^2

# imports
import numpy
from pandas import read_csv
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

# load data
filename = 'pima-indians-diabetes.data.csv'
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi',
         'age', 'class']
dataframe = read_csv(filename, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]

## Classification Accuracy
# is the number of correct predictions made as a ratio of all
# predictions made. This is the most common evaluation for
# claassification problems, it is also most misued. It is really only
# suitable when there are an equal number of observations in each
# class and that all prediction errors are equally important.
kfold = KFold(n_splits=10, random_state=7)
model = LogisticRegression()
scoring = 'accuracy'
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("Accuracy: %.3f (%.3f)") % (results.mean(), results.std())

## Logarithmic Loss
# is performance metric for evaluating the predictions of
# probabilities of membership to a given class. The scalar probability
# between 0 and 1 can be seen as a measure of confidence for a
# prediction by an algorithm.
scoring = 'neg_log_loss'
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("Logloss: %.3f (%.3f)") % (results.mean(), results.std())

## Area Under ROC Curve - AUC
# is a performance metric for binary classification problems.The AUC
# represents a model's ability to disciminate between positive and
# negative classes. An area of 1.0 represents a model that made all
# predictions perfectly. An area of 0.5 represents a model that is as
# good as rando. A binary classification problem is really a trade-off
# between sensitivity (true positives/also recall) and specificity
# (true negatives).
scoring = 'roc_auc'
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("AUC: %.3f (%.3f)") % (results.mean(), results.std())

## Confusion Matrix
# is a handy presentation of the accuracy of a model with two or more
# classes. The table presents predictions on the x-axis and accuracy
# outcomes on the y-axes. The cells of the table are the number of
# predictions made by a machine learning algorithm.
test_size = 0.33
seed = 7
X_train, X_test, Y_train, Y_test = train_test_split(X, Y,
                                                    test_size=test_size,
                                                    random_state=seed)
model = LogisticRegression()
model.fit(X_train, Y_train)
predicted = model.predict(X_test)
matrix = confusion_matrix(Y_test, predicted)
print(matrix)

## Classification Report
# scikit-learn library provides a report of the accuracy of a model
# using a number of measures
report = classification_report(Y_test, predicted)
print(report)


## Regression Metrics
## Mean Aboslute Error - MAE
# a value of 0 means no error or prefect prediction
names = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE',
                 'DIS', 'RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV']
dataframe = read_csv("https://goo.gl/KQGf9g", delim_whitespace=True,
                     names=names)
array = dataframe.values
X = array[:, numpy.array([0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13])]
Y = array[:, 8]
kfold = KFold(n_splits=10, random_state=7)
model = LogisticRegression()
scoring = 'neg_mean_absolute_error'
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("MAE: %.3f (%.3f)") % (results.mean(), results.std())

## Mean Squared Error - MSE
# is much like MAE in that it provides a gross idea of the magnitude
# of error. Taking the square root of the MSE converts the units back
# to the original units of the output variable and can be meaningful
# for description and presentation. This is called the Root Mean
# Squared Error (or RMSE).
kfold = KFold(n_splits=10, random_state=7)
model = LogisticRegression()
scoring = 'neg_mean_squared_error'
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("MSE: %.3f (%.3f)") % (results.mean(), results.std())

## R Squared
# provides an indication of the goodness of fit of a set of
# predictions to the actual values. In statistical literature this
# measure is called the coefficient of determination. This is a value
# between 0 and 1 for no-fit and perfect fit respectively.
kfold = KFold(n_splits=10, random_state=7)
model = LinearRegression()
scoring = 'r2'
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("R^2: %.3f (%.3f)") % (results.mean(), results.std())

