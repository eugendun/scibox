import numpy
import pandas

# series
print('series')
myarray = numpy.array([1,2,3])
rownames = ['a', 'b', 'c']
myseries = pandas.Series(myarray, index=rownames)
print(myseries[0])
print(myseries[1])
print(myseries['b'])

# dataframe
print('dataframe')
myarray = numpy.array([[1,2,3],[4,5,6]])
rownames = ['a','b']
colnames = ['one', 'two', 'three']
mydataframe = pandas.DataFrame(myarray, index=rownames, columns=colnames)
print(mydataframe)

print("method 1:")
print("one column: %s") % mydataframe['one']
print("method 2:")
print("one columne: %s") % mydataframe.one
