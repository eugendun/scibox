## Split up training dataset and create useful estimates of
# performance for machine learning algorithms
# Four techniques:
# - Train and Tests Sets
# - k-fold Cross Validation
# - Leave One Out Cross Validation
# - Repeated Random Test-Train Splits

## imports
from pandas import read_csv
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import LeaveOneOut
from sklearn.model_selection import ShuffleSplit

## load data
filename = 'pima-indians-diabetes.data.csv'
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi',
         'age', 'class']
dataframe = read_csv(filename, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]

## Split into Train and Test Sets
test_size = 0.33
seed = 7
X_train, X_test, Y_train, Y_test = train_test_split(X, Y,
                                                    test_size=test_size,
                                                    random_state=seed)
model = LogisticRegression()
model.fit(X_train, Y_train)
result = model.score(X_test, Y_test)
print("Accuracy: %.3f%%") % (result*100.0)

## K-fold Cross Validation
# is an approach you can use to estimate the performance of a machine
# learning algorithm with less variance than a single train-test set
# split. It works by splitting the dataset into k-parts. Each split of
# the data is called a fold. The algorithm is trained on k-1 folds
# with one held back and tested on the held back fold.
num_folds = 10
seed = 7
kfold = KFold(n_splits=num_folds, random_state=seed)
model = LogisticRegression()
results = cross_val_score(model, X, Y, cv=kfold)
print("Accuracy: %.3f%% (%.3f%%)") % (results.mean()*100.0,
                                      results.std()*100.0)

## Leave One Out Cross Validation
# Is a special case of K-fold Cross Validation with K=1. The results
# is a large number of performance measures that can be summarized in
# an effort to give a more reasonable estimate of the accuracy of your
# model on unseen data. (expensive)
loocv = LeaveOneOut()
results = cross_val_score(model, X, Y, cv=loocv)
print("Accuracy: %.3f%% (%.3f%%)") % (results.mean()*100.0,
                                      results.std()*100.0)

## Repeated Random Test-Train Splits
# Repeat the process of splitting and evaluation of the algorithm multiple times.
n_splits = 10
test_size = 0.33
seed = 7
kfold = ShuffleSplit(n_splits=n_splits, test_size=test_size, random_state=seed)
results = cross_val_score(model, X, Y, cv=kfold)
print("Accuracy: %.3f%% (%.3f%%)") % (results.mean()*100.0,
                                      results.std()*100.0)
