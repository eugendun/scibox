from pandas import read_csv
filename = 'pima-indians-diabetes.data.csv'
names = ['preg', 'plas', 'pres', 'skin',
         'test', 'mass', 'pedi', 'age', 'class']
dataframe = read_csv(filename, names=names)
array = dataframe.values
X = array[:, 0:8]
Y = array[:, 8]
print(dataframe.shape)

# using url
from pandas import read_csv
url = 'https://goo.gl/vhm1eU'
names = ['preg', 'plas','pres','skin','test','mass','pedi','age','class']
data = read_csv(url, names = names)
print(data.shape)
