# Create a pipeline that standardizes the data then creates a model
# imports
from pandas import read_csv
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.pipeline import FeatureUnion
from sklearn.linear_model import LogisticRegression

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot

# load data
filename = 'pima-indians-diabetes.data.csv'
names = ['preg', 'plas', 'pres', 'skin',
         'test', 'mass', 'pedi', 'age', 'class']
dataframe = read_csv(filename, names=names)
array = dataframe.values
X = array[:, 0:8]
Y = array[:, 8]

# create pipeline
estimators = []
estimators.append(('standardizes', StandardScaler()))
estimators.append(('lda', LinearDiscriminantAnalysis()))
model = Pipeline(estimators)

results = []

# evaluate pipeline
kfold = KFold(n_splits=10, random_state=7)
results.append(cross_val_score(model, X, Y, cv=kfold))
# print(results.mean())

# Create a pipeline that extracts features from the data then creates a model
# create feature union
features = []
features.append(('pca', PCA(n_components=3)))
features.append(('select_best', SelectKBest(k=6)))
feature_union = FeatureUnion(features)
# create pipeline
estimators = []
estimators.append(('feature_union', feature_union))
estimators.append(('LR', LogisticRegression()))
model = Pipeline(estimators)

# evaluate pipeline
kfold = KFold(n_splits=10, random_state=7)
results.append(cross_val_score(model, X, Y, cv=kfold))
# print(results.mean())

# LR without PCA
features = []
features.append(('pca', PCA(n_components=3)))
features.append(('select_best', SelectKBest(k=6)))
feature_union = FeatureUnion(features)
# create pipeline
estimators = []
#estimators.append(('feature_union', feature_union))
estimators.append(('LR', LogisticRegression()))
model = Pipeline(estimators)

# evaluate pipeline
kfold = KFold(n_splits=10, random_state=7)
results.append(cross_val_score(model, X, Y, cv=kfold))
# print(results.mean())

print("%s: %f") % ('LDA', results[0].mean())
print("%s: %f") % ('LR with PCA-3', results[1].mean())
print("%s: %f") % ('LR without PCA', results[2].mean())

# Box and Whiskers plots
fig = pyplot.figure()
fig.suptitle('LDA and LR - pipes')
ax = fig.add_subplot(111)
pyplot.boxplot(results)
ax.set_xticklabels(['LDA', 'LR-PCA-3', 'LR'])
fig.savefig('LdaAndLrPipes.png')
