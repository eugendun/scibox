## Regression Alogrithms Overview
#  four linear machine learning algorithms
#   * Linear Regresssion
#   * Ridge Regression
#   * LASSO Linear Regression
#   * Elastic Net Regression
#  three nonlinear machine learning algorithms
#   * k-Nearest Neighbors
#   * Classification and Regression Trees
#   * Support Vector Machines

# imports
import numpy
from pandas import read_csv
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.linear_model import ElasticNet
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR

# load data
names = ['CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE',
                 'DIS', 'RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV']
dataframe = read_csv("https://goo.gl/KQGf9g", delim_whitespace=True,
                     names=names)
array = dataframe.values
X = array[:, numpy.array([0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13])]
Y = array[:, 8]
kfold = KFold(n_splits=10, random_state=7)

## Linear Regression
# assumes that the input variables have a Gaussion distribution. It is
# also assumed that input variables are relevant to the output
# variable and that they are not highly correlated with each other.
model = LinearRegression()
scoring = 'neg_mean_squared_error'
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print(results.mean())

## Ridge Regression
# is an extension of linear regression where the loss function is
# modified to minimize the complexity of the model measured as the sum
# squared value of the coefficient values (L2-norm).
model = Ridge()
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print(results.mean())

## LASSO Regression
# Least Absolute Shrinkage and Selection Operator is a modification of
# linear regression, like ridge regression, where the loss function is
# modified to minimize the complexity of the model measured as the sum
# absolute value of the coefficient values (L1-norm).
model = Lasso()
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print(results.mean())

## ElasticNet Regression
# is a form of regularization regression that combines the properties
# of both Ridge Regression and LASSO regression. It seeks to minimize
# the complexity of the regression model by penalizing the model using
# both the L2-norm and the L1-norm.
model = ElasticNet()
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print(results.mean())

## K-Nearest Neighbors - KNN
# locates the k most similar instances in the training dataset for a
# new data instance. From the k neighbors, a mean or median output
# variable is taken as the prediction. Of note is the distance metric
# used (the metric argument). The Minkowski distance is used by
# default, which is a generalization of the Euclidean distance and
# Manhatten distance.
model = KNeighborsRegressor()
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print(results.mean())

## Classification and Regression Trees - CART
# use the training data to select the best points to split the data in
# order to minimize a cost metric. The default cost metric for
# regression decision trees is the mean squared error, specified in
# the criterion parameter.
model = DecisionTreeRegressor()
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print(results.mean())

## Support Vector Machines - SVM/SVR
# SVM were developed for binary classification but has been extended
# for the prediction real-valued problems call Support Vector
# Regression.
model = SVR()
results = cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print(results.mean())
