# imports
from pandas import read_csv
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import BaggingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import VotingClassifier

# load data
filename = 'pima-indians-diabetes.data.csv'
names = ['preg', 'plas', 'pres', 'skin', 'test',
         'mass', 'pedi', 'age', 'class']
dataframe = read_csv(filename, names=names)
array = dataframe.values
X = array[:, 0:8]
Y = array[:, 8]

# Bagged Decision Trees - Bagging performs best with algorithms that
# have high variance. A popular example are decision trees, often
# constructed without pruning.
seed = 7
kfold = KFold(n_splits=10, random_state=seed)
cart = DecisionTreeClassifier()
num_trees = 100
model = BaggingClassifier(base_estimator=cart, n_estimators=10,
                          random_state=seed)
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

# Random Forest - is an extension bagged decision tress. Samples of
# the training dataset are taken with replacement, but the trees are
# constructed in a way that reduces the correlation between individual
# classifiers.
max_features = 3
model = RandomForestClassifier(n_estimators=num_trees,
                               max_features=max_features)
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

# Extra Trees - are another modification of bagging where random trees
# are constructed from samples of the training dataset.
model = ExtraTreesClassifier(n_estimators=num_trees, max_features=max_features)
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

# AdaBoost Classification - was perhaps the first successful boosting
# ensemble algorithm. It generally works by weighting instances in the
# dataset by how easy or difficult they are to classify, allowing the
# algorithm to pay or less attention to them in the construction of
# subsequent models.
num_trees = 30
model = AdaBoostClassifier(n_estimators=num_trees, random_state=seed)
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

# Stochastic Gradient Boosting - (also called Gradient Boosting
# Machine) are one oth the most sophisticated ensemble techniques. It
# is also a technique that is proving to be perhaps one of the best
# techniques available for improving performance via ensembles.
num_trees = 100
model = GradientBoostingClassifier(n_estimators=num_trees, random_state=seed)
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())

# Voting Ensemble - is one of the simplest ways of combining the
# predications from multiple machine learning algorithms.
estimators = []
estimators.append(('logistic', LogisticRegression()))
estimators.append(('cart', DecisionTreeClassifier()))
estimators.append(('svm', SVC()))
ensemble = VotingClassifier(estimators)
results = cross_val_score(ensemble, X, Y, cv=kfold)
print(results.mean())
